const OAuth2Strategy = require('passport-oauth2');
// const uri = require('url');

class Strategy extends OAuth2Strategy {
  constructor(options, verify) {
    const new_options = {
      authorizationURL: 'https://oauth.vk.com/authorize',
      tokenURL: 'https://oauth.vk.com/access_token',
      version: '5.69',
      scope: ['manage'],
      response_type: 'code',
      ...options,
    };

    super(new_options, verify);

    this.options = new_options;
    this.name    = 'vkontakte-group';
  }

  /**
   * Return extra parameters to be included in the authorization request.
   *
   * Some OAuth 2.0 providers allow additional, non-standard parameters to be
   * included when requesting authorization.  Since these parameters are not
   * standardized by the OAuth 2.0 specification, OAuth 2.0-based authentication
   * strategies can overrride this function in order to populate these parameters
   * as required by the provider.
   *
   * @param {Object} options
   * @return {Object}
   * @api protected
   */
  authorizationParams(options) {
    if (!options.group_ids) throw new Error('[PASSPORT-VK-GROUP] group_ids must be defined!');
    return {
      group_ids: options.group_ids,
      version: this.version,
      ...options,
    };
  }

  // /**
  //  * Return extra parameters to be included in the token request.
  //  *
  //  * Some OAuth 2.0 providers allow additional, non-standard parameters to be
  //  * included when requesting an access token.  Since these parameters are not
  //  * standardized by the OAuth 2.0 specification, OAuth 2.0-based authentication
  //  * strategies can overrride this function in order to populate these parameters
  //  * as required by the provider.
  //  *
  //  * @return {Object}
  //  * @api protected
  //  */
  // tokenParams(options) {
  //   return {};
  // }
}

module.exports = Strategy;
